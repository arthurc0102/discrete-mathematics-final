import sys


def decrypt(cipher_text, k):
    result = ''

    for c in cipher_text:
        code = (ord(c) if c != ' ' else 64) - k

        if code == 64:
            result += ' '
            continue

        if code < 65:
            code += 27

        if code > 90:
            code -= 27

        result += chr(code)

    return result


def search_dictionary(dictionary, plain_text):
    return [word in dictionary for word in plain_text.split(' ')].count(True)


def output(plan_text):
    result = ''
    line_len = 0

    for word in plan_text.split(' '):
        if not word:
            continue

        if not result:
            result += word
            line_len = len(word)
            continue

        join_char = ' '
        if line_len + len(word) > 59:  # 59 是因為字與字中間需要有空白
            join_char = '\n'
            line_len = 0

        result = '{}{}{}'.format(result, join_char, word)
        line_len += len(word) + (1 if join_char == ' ' else 0)

    print(result)


def main():
    data = [line.replace('\n', '') for line in sys.stdin if line]
    *dictionary, _, cipher_text = data

    result = []
    for i in range(1, 27):
        plain_text = decrypt(cipher_text, i)
        word_in_dictionary = search_dictionary(dictionary, plain_text)

        if word_in_dictionary < 1:
            continue

        result.append((word_in_dictionary, plain_text))

    result.sort(key=lambda x: x[0], reverse=True)
    output(result[0][1])


if __name__ == '__main__':
    main()
