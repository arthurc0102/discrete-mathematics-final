import sys


def main():
    result_list = []

    for data in sys.stdin:
        year = int(data.replace('\n', ''))

        is_leap = (year % 4 == 0 and year % 100 != 0) or (year % 400 == 0)
        is_huluculu = year % 15 == 0
        is_bulukulu = (year % 55 == 0) and is_leap

        result = []
        if not any([is_leap, is_huluculu, is_bulukulu]):
            result.append('This is an ordinary year.')

        if is_leap:
            result.append('This is leap year.')

        if is_huluculu:
            result.append('This is huluculu festival year.')

        if is_bulukulu:
            result.append('This is bulukulu festival year.')

        result_list.append(result)

    print('\n\n'.join(['\n'.join(result) for result in result_list]))


if __name__ == '__main__':
    main()
