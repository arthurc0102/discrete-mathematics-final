import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Scanner;

public class Main {
    private static ArrayList<Integer> data = new ArrayList<>();
    private static ArrayList<Integer> runaroundNumbers = new ArrayList<>();

    public static void main(String[] args) {
        getData();
        getRunaroundNumbers();

        for (int i = 0; i < data.size(); i++) {
            for (int number: runaroundNumbers) {
                if (number >= data.get(i)) {
                    System.out.printf("Case %d: %d\n", i + 1, number);
                    break;
                }
            }
        }
    }

    private static void getData() {
        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNextInt()) {
            int number = scanner.nextInt();

            if (number == 0) {
                break;
            }

            data.add(number);
        }
    }

    private static void getRunaroundNumbers() {
        int start = Collections.min(data);
        int stop = Collections.max(data);

        outer: for (int i = start; i < 10000000; i++) {
            ArrayList<Integer> target = new ArrayList<>();

            for (char c: String.valueOf(i).toCharArray()) {
                target.add(Character.getNumericValue(c));
            }

            if (target.size() != (new HashSet<>(target)).size()) {
                continue;
            }

            int now = 0;

            for (int j = 0; j < (target.size() + 1); j++) {
                int step = target.get(now);

                if (step == -1) {
                    continue outer;
                }

                if (j != 0) {
                    target.set(now, -1);
                }

                now += step;
                while (now >= target.size()) now %= target.size();
            }

            for (int t: target) {
                if (t != -1) {
                    continue outer;
                }
            }

            runaroundNumbers.add(i);

            if (i >= stop) {
                return;
            }
        }
    }
}
