import sys


def find_runaround_numbers(start, stop):
    result = []

    for i in range(start, 10000000):
        is_runaround = False
        target = [int(c) for c in str(i)]
        target_len = len(target)

        if len(set(target)) != target_len:
            continue

        now = 0
        for run_c in range(target_len+1):
            step = target[now]
            if step == -1:
                break

            if run_c != 0:
                target[now] = -1

            now += step

            while now >= target_len:
                now %= target_len
        else:
            if not all([t == -1 for t in target]):
                continue

            result.append(i)
            is_runaround = True

        if i >= stop and is_runaround:
            break

    return result


def main():
    data = [int(d.replace('\n', '')) for d in sys.stdin if d][:-1]
    runaround_numbers = find_runaround_numbers(min(data), max(data))

    for i, d in enumerate(data, start=1):
        for number in runaround_numbers:
            if number >= d:
                print('Case {}: {}'.format(i, number))
                break


if __name__ == '__main__':
    main()
