# Discrete Mathematics Final

> 離散數學期末

- UVa-347: <https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=641&page=show_problem&problem=283>
- UVa-554: <https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=7&page=show_problem&problem=495>
- UVa-10070: <https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=619&page=show_problem&problem=1011>
